const MarkdownIt = require('markdown-it');

export default function render(input:string) {
  const md: any = MarkdownIt({
    html: true,
  });
  var result = md.render(input);
  return result;
}
